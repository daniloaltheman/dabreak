//
//  ContentView.swift
//  DABreak
//
//  Created by Danilo Altheman on 05/11/20.
//

import SwiftUI

struct ContentView: View {
    @State private var tocou: String = ""
    var body: some View {
        VStack {
            Text("\(tocou)").padding()
            
            Button("Clique-me") {
                self.tocou = "Sim, tocou"
            }
            Text("Ovo")
        }
        
        

        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
