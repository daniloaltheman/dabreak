//
//  DABreakApp.swift
//  DABreak
//
//  Created by Danilo Altheman on 05/11/20.
//

import SwiftUI

@main
struct DABreakApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
